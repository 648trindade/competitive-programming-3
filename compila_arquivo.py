#!/usr/bin/python3

from sys import argv, exit
from os import system

if len(argv) < 2:
    print ("USO:",argv[0],"<Nº Problema>","[Pasta]")
    exit(1)

arq = "UVa_" + argv[1] + ".cpp"
exe = argv[1]

if (len(argv) is 3):
    arq = argv[2] + "/" + arq
    exe = argv[2] + "/" + exe


comp = "g++ " + arq + " -std=c++11 -O2 -lm -o " + exe

system(comp)