#!/usr/bin/python3

from sys import argv, exit

cabecalho = """#include <iostream>
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long       ll;
typedef pair<int, int>  ii;
typedef vector<ii>      vii;
typedef vector<int>     vi;
#define INF 1000000000

// UVa XXXXX - 
// 

int main(){
    
    return 0;
}"""

if len(argv) < 2:
    print ("USO:",argv[0],"<Nº Problema>","[Pasta]")
    exit(1)

arq = "UVa_" + argv[1] + ".cpp"

if (len(argv) is 3):
    arq = argv[2] + "/" + arq

with open(arq,"w") as source:
    source.write(cabecalho.replace("XXXXX",argv[1]))
    print("Arquivo",arq,"criado com sucesso.")