#include <iostream>
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long       ll;
typedef pair<int, int>  ii;
typedef vector<ii>      vii;
typedef vector<int>     vi;
#define INF 1000000000

// UVa 11044 - Searching for Nessy
// https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=1985

int main(){
    int t, n, m, nn, mm;
    cin >> t;
    for(int i=0; i<t; i++){
        cin >> n >> m;
        n = ((n-2)/3) + (((n-2)%3)>=0);
        m = ((m-2)/3) + (((m-2)%3)>=0);
        cout << n*m << endl;
    }
    return 0;
}