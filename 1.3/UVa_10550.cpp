#include <iostream>
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long       ll;
typedef pair<int, int>  ii;
typedef vector<ii>      vii;
typedef vector<int>     vi;
#define INF 1000000000

// UVa 10550 - Combination Lock (Simple, do as asked)
// https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=1491

int main(){
    int total;
    int pos;
    int p1, p2, p3;
    while(scanf("%d %d %d %d",&pos,&p1,&p2,&p3)!=EOF){
        if (!(pos || p1 || p2 || p3))
            break;
        total = 540;
        total += ((pos-p1)%40)*9;
        total += ((p2-p1)%40)*9;
        total += ((p2-p3)%40)*9;
        printf("%d\n",total);
    }
    return 0;
}