#include <iostream>
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long       ll;
typedef pair<int, int>  ii;
typedef vector<ii>      vii;
typedef vector<int>     vi;
#define INF 1000000000

// UVa 01124 - Celebrity Jeopardy (LA 2681, just echo/reprint the input again)
// https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=3565

int main(){
    char line[80];
    while(scanf(" %[^\n]",line)!=EOF)
        printf("%s\n",line);
    return 0;
}