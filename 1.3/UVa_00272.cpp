#include <iostream>
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long       ll;
typedef pair<int, int>  ii;
typedef vector<ii>      vii;
typedef vector<int>     vi;
#define INF 1000000000

// UVa 00272 - TEX Quotes (replace all double quotes to TEX() style quotes)
// https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=208

int main(){
    char c;
    int start = 1;
    while(scanf("%c",&c)!=EOF){
        if (c=='"'){
            if (start) printf("``");
            else printf("''");
            start = !start;
        }
        else printf("%c",c);
    }
    return 0;
}
