# README #

Vamos... trabalhar

### What is this repository for? ###

* Treinamento Maratona de Programação SBC
* Exercícios UVa do livro Competitive Programming 3

### How do I get set up? ###

* Ambiente GNU/Linux
* Python 3
* g++

### Contribution guidelines ###

* resolvam os problemas que conseguirem e coloquem aqui

### Como rodar os scripts? ###

####Gerador de Arquivo####

*./gera_arquivo.py <No_problema> [Pasta]*

Gera um arquivo UVa_XXXXX.cpp com os includes comumente necessários, onde:

* *No_problema:*   número do problema no UVa (obrigatório)
* *Pasta:*         pasta onde o arquivo será salvo (opcional)

####Compilador####

*./compila_arquivo.py <No_problema> [Pasta]*

Compila um arquivo UVa_XXXXX.cpp e gera um executável XXXXX, onde:

* *No_problema:*   número do problema no UVa (obrigatório)
* *Pasta:*         pasta onde o arquivo será lido e o executável salvo (opcional)